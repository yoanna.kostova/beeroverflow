﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Models
{
    public class Country
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2)]
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ISO { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<Beer> Beers { get; set; } = new List<Beer>();
        public ICollection<Brewery> Breweries { get; set; } = new List<Brewery>();
    }
}
