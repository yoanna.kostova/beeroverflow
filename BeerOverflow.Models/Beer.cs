﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BeerOverflow.Models
{
    public class Beer
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(40, MinimumLength = 2)]
        public string Name { get; set; }

        [StringLength(500, MinimumLength = 10)]
        public string Description { get; set; }
        public DateTime CreatedOn { get; set; }
        public double Price { get; set; }
        public string ImageURL { get; set; }
        public bool IsBeerOfTheWeek { get; set; }
        public bool IsProduced { get; set; }
        public double AlcoholPercent { get; set; }
        public bool IsPublished { get; set; }
        public int Mililiters { get; set; }
        public bool IsDeleted { get; set; }
        public Style Style { get; set; }
        public Guid StyleId { get; set; }
        public Brewery Brewery { get; set; }
        public Guid BreweryId { get; set; }
        public Country Country { get; set; }
        public Guid CountryId { get; set; }
        public ICollection<Review> Reviews { get; set; } = new List<Review>();
        public ICollection<Wishlist> Wishlists { get; set; } = new List<Wishlist>();
    }
}
