﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Models
{
    public class User : IdentityUser<Guid>
    {  
        public DateTime CreatedOn { get; set; }
        public bool IsBanned { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsAdmin { get; set; }

        //public ICollection<Review> Reviews { get; set; } = new List<Review>(); 
        public ICollection<Wishlist> Wishlist { get; set; } = new List<Wishlist>();
    }
}
