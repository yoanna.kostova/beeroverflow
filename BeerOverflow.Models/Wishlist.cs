﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Models
{
    public class Wishlist
    {
        [Key]
        public Guid Id { get; set; }
        public Guid BeerId { get; set; }
        public Beer Beer { get; set; }
        public Guid UserId { get; set; }
        public User User { get; set; }
    }
}
