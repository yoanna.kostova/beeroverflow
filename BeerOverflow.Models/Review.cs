﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Models
{
    public class Review
    {
        [Key]
        public Guid Id { get; set; }

        [StringLength(400, MinimumLength = 20)]
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Flagged { get; set; }
        public bool IsBanned { get; set; }
        public int Rating { get; set; }
        public bool IsDeleted { get; set; }
        //public User User { get; set; }
        //public Guid? UserId { get; set; }
        public Beer Beer { get; set; }
        public Guid BeerId { get; set; }
    }
}
