﻿using BeerOverflow.Database;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.ServiceTests.BeerServiceTests
{
    [TestClass]
    public class CreateBeer_Should
    {

        [TestMethod]
        public void Successful_CreateBeer()
        {
            var options = TestUtilities.GetOptions("CorrectlyCreateBeer");

            var beer = new BeerDTO
            {
                Id = Guid.NewGuid(),
                AlcoholPercent = 3,
                Description = "this is a great beer and I love it!",
                ImageURL = null,
                IsBeerOfTheWeek = false,
                IsDeleted = false,
                IsProduced = true,
                IsPublished = true,
                Mililiters = 330,
                Name = "Heineken",
                Price = 3.40
            };

            using (var testContext = new BeerOverflowDbContext(options))
            {
                //sut-system under test
                var sut = new BeerServices(testContext, new StyleServices(testContext), new CountryServices(testContext), new
                    BreweryServices(testContext));

                var result = sut.CreateBeer(beer);

                Assert.IsInstanceOfType(result, typeof(BeerDTO));
                Assert.AreEqual("3", result.AlcoholPercent);
                Assert.AreEqual("this is a great beer and I love it!", result.Description);

            }

        }
    }
}
