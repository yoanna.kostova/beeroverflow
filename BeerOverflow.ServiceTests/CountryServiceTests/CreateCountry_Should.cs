﻿using BeerOverflow.Database;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.Services;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.ServiceTests.CountryServiceTests
{
    [TestClass]
    public class CreateCountry_Should
    {
        [TestMethod]
        public void Successful_CreatesCountry()
        {
            var options = TestUtilities.GetOptions("Successful_CreatesCountry");

            var country = new CountryDTO
            {
                Id = Guid.NewGuid(),
                IsDeleted = false,
                ISO = "BG",
                Name = "Bulgaria",
                Users = null,
                Beers = null,
                Breweries = null
            };

            using(var testContext = new BeerOverflowDbContext(options))
            {
                //sut-system under test
                var sut = new CountryServices(testContext);

                var result = sut.CreateCountry(country);

                Assert.IsInstanceOfType(result, typeof(CountryDTO));
                Assert.AreEqual("Bulgaria", result.Name);
                Assert.AreEqual(country.Breweries, result.Breweries);
                Assert.AreEqual(country.Beers, result.Beers);
                Assert.AreEqual(country.Users, result.Users);
                Assert.AreEqual(country.IsDeleted, result.IsDeleted);
            }
        }

        [TestMethod]
        public void ExceptionThrown_WhenNull_CreateCountry()
        {
            var options = TestUtilities.GetOptions(nameof(ExceptionThrown_WhenNull_CreateCountry));


            using (var testContext = new BeerOverflowDbContext(options))
            {
                var sut = new CountryServices(testContext);

                Assert.ThrowsException<Exception>(() => sut.CreateCountry(null));
            }
        }
    }
}
