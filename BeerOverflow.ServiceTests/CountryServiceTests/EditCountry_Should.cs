﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.ServiceTests.CountryServiceTests
{
    [TestClass]
    public class EditCountry_Should
    {
        [TestMethod]
        public void Successfully_EditCountry()
        {

            var options = TestUtilities.GetOptions(nameof(Successfully_EditCountry));

            var country = new CountryDTO
            {
                Id = Guid.Parse("ef607ee6 - ebb3 - 4722 - 9b70 - 7d5238e98f43"),
                IsDeleted = false,
                ISO = "BG",
                Name = "Bulgaria"
            };

            var brewery = new BreweryDTO()
            {
                Id = Guid.Parse("b5797feb-c8a7-45d9-829f-7c5a397a1728"),
                CountryId = Guid.Parse("ef607ee6 - ebb3 - 4722 - 9b70 - 7d5238e98f43"),
                CountryName = "Bulgaria",
                Name = "Brew"
            };

            using (var assertContext = new BeerOverflowDbContext(options))
            {
                var countryService = new CountryServices(assertContext);

                var countryEntity = new Country
                {
                    Id = Guid.Parse("ef607ee6 - ebb3 - 4722 - 9b70 - 7d5238e98f43"),
                    IsDeleted = false,
                    ISO = "BG",
                    Name = "Bulgaria",
                    Breweries = null,
                    Beers = null
                };

                assertContext.Add(country);
                assertContext.SaveChanges();

                var result = countryService.GetCountry("Bulgaria");

                Assert.AreEqual(countryEntity.Id, result.Id);
                Assert.AreEqual(countryEntity.Name, result.Name);
                Assert.AreEqual(countryEntity.ISO, result.ISO);

            }
        }
    }
}
