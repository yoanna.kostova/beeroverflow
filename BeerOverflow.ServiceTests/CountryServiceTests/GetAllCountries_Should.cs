﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using BeerOverflow.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.ServiceTests.CountryServiceTests
{
    [TestClass]
    public class GetAllCountries_Should
    {
        [TestMethod]
        public void Successful_ReturnCollectionAllCountries()
        {
            var options = TestUtilities.GetOptions(nameof(Successful_ReturnCollectionAllCountries));

            var country = new CountryDTO
            {
                Id = Guid.Parse("ef607ee6 - ebb3 - 4722 - 9b70 - 7d5238e98f43"),
                IsDeleted = false,
                ISO = "BG",
                Name = "Bulgaria"
            };

            var brewery = new BreweryDTO()
            {
                Id = Guid.Parse("b5797feb-c8a7-45d9-829f-7c5a397a1728"),
                CountryId = Guid.Parse("ef607ee6 - ebb3 - 4722 - 9b70 - 7d5238e98f43"),
                CountryName = "Bulgaria",
                Name = "Brew"
            };

            using (var assertContext = new BeerOverflowDbContext(options))
            {
                var mockCountryService = new CountryServices(assertContext);

                var countryEntity = new Country
                {
                    Id = Guid.Parse("ef607ee6 - ebb3 - 4722 - 9b70 - 7d5238e98f43"),
                    IsDeleted = false,
                    ISO = "BG",
                    Name = "Bulgaria",
                    Breweries = null,
                    Beers = null
                };

                assertContext.Add(country);

                var result = mockCountryService.GetAllCountries().ToList();

                Assert.AreEqual(countryEntity.Id, result[0].Id);
                Assert.AreEqual(countryEntity.Name, result[0].Name);
                Assert.AreEqual(countryEntity.Beers, result[0].Beers);
                Assert.AreEqual(countryEntity.Breweries, result[0].Breweries);
            }
        }

        [TestMethod]
        public void Throws_When_CreateCountry_EntityNull()
        {

            var options = TestUtilities.GetOptions(nameof(Throws_When_CreateCountry_EntityNull));

            using (var assertContext = new BeerOverflowDbContext(options))
            {
                var countryService = new CountryServices(assertContext);

                Assert.ThrowsException<Exception>(() => countryService.GetAllCountries());
            }
        }

    }
}
