﻿using BeerOverflow.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.ServiceTests
{
    [TestClass]
    public class TestUtilities
    {
        [TestMethod]
        public static DbContextOptions<BeerOverflowDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<BeerOverflowDbContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
