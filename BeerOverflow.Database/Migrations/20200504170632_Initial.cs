﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BeerOverflow.Database.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsBanned = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Countries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ISO = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Countries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Styles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Styles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<Guid>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Breweries",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Breweries", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Breweries_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Beers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 40, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    ImageURL = table.Column<string>(nullable: true),
                    IsBeerOfTheWeek = table.Column<bool>(nullable: false),
                    IsProduced = table.Column<bool>(nullable: false),
                    AlcoholPercent = table.Column<double>(nullable: false),
                    IsPublished = table.Column<bool>(nullable: false),
                    Mililiters = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    StyleId = table.Column<Guid>(nullable: false),
                    BreweryId = table.Column<Guid>(nullable: false),
                    CountryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Beers_Breweries_BreweryId",
                        column: x => x.BreweryId,
                        principalTable: "Breweries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Beers_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Beers_Styles_StyleId",
                        column: x => x.StyleId,
                        principalTable: "Styles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reviews",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Content = table.Column<string>(maxLength: 400, nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Flagged = table.Column<int>(nullable: false),
                    IsBanned = table.Column<bool>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    BeerId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reviews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reviews_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Wishlist",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BeerId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wishlist", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Wishlist_Beers_BeerId",
                        column: x => x.BeerId,
                        principalTable: "Beers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Wishlist_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { new Guid("cdde12b9-e61a-4748-a239-c7331b4fb6a8"), "a07071c4-cc9b-4651-bbf8-b02210fe40f3", "Admin", "ADMIN" },
                    { new Guid("43a08cc4-76ae-46d8-9e2c-cde7b0479146"), "2ed3c7d7-ad3d-4843-a0eb-e3b429ff7993", "Member", "MEMBER" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "IsAdmin", "IsBanned", "IsDeleted", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { new Guid("cf7b1d39-81a3-49ae-b5a5-40e2633c331b"), 0, "8fa9f4ad-2b93-4bb4-9951-9f62727d9c83", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "regina@felangie.com", false, false, false, false, false, null, "REGINA@FELANGIE.COM", "REGINA@FELANGIE.COM", "AQAAAAEAACcQAAAAEBwt+7rFMero95Pf5RDzLIP7Lo/hdE6VAxbUTqmCfb2prmfc4iJZxFLk5zBVp0LHIA==", null, false, "15CLJEKQCTLPRXMVXXNSWXZH6R6KJRRU", false, "regina@felangie.com" },
                    { new Guid("8d421895-bdc9-46b0-8ced-f7d86bef8f24"), 0, "1bea4570-c19a-4b48-b11e-6a5bb942635b", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "satoshi@nakamoto.com", false, true, false, false, false, null, "SATOSHI@NAKAMOTO.COM", "SATOSHI@NAKAMOTO.COM", "AQAAAAEAACcQAAAAEC5K4LMLfUVUwhRxcPNUcr9kcj9GzP/XXUq6Fa2nt/wnYEqQLyQ+cxeoySWCfobRjA==", null, false, "7I5VNHIJTSZNOT3KDWKNFUV5PVYBHGXN", false, "satoshi@nakamoto.com" }
                });

            migrationBuilder.InsertData(
                table: "Countries",
                columns: new[] { "Id", "CreatedOn", "ISO", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("eee1a9ab-c409-42c4-ae07-f622a959bb0b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, "Bulgaria" },
                    { new Guid("f444594e-5626-4c1e-b285-571f33930010"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, "Serbia" },
                    { new Guid("e0932858-11cb-4a27-87f2-8756649b8c86"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, "Germany" },
                    { new Guid("56e91350-598b-4f55-94d9-eefd329f1861"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, "Romania" },
                    { new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, "Unknown" }
                });

            migrationBuilder.InsertData(
                table: "Styles",
                columns: new[] { "Id", "CreatedOn", "Description", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The classic Pale Ale is generally a deep-golden to copper colored, hop-forward ale with a balanced malt profile. This style specifically represents all generic Pale Ales (sometime called International Pale Ale) which are marketed as such and which cannot be defined as a specific regional Pale Ale style such as the American Pale Ale. This also includes beers marketed as Extra Pale Ale (XPA), a non-defined style that usually sits between an American Pale Ale and an India Pale Ale, a hop forward beer and generally more intense than an APA but not as hop-forward as an IPA. Sometimes, the XPA also refers to a session-strength or simply paler Pale Ale.", false, "Pale Ale" },
                    { new Guid("f32de916-9ea8-4f93-96d2-732d1b01fe8e"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, false, "Unknown" },
                    { new Guid("96cc020a-753d-4483-9531-02b0a83b0a66"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Brut India Pale Ale (IPA) is a very pale to light golden, very dry, highly effervescent variant of American IPA, usually highly hopped with aromatic hops, but with far less actual bitterness. Enzymes are used in the mash and/or fermenter along with highly fermentable wort and often adjuncts like rice and corn to achieve close to total attenuation, resulting in an absent residual malt sweetness. Hopped in a similar fashion to New England IPA, but without sweetness. Pale like a West Coast IPA, but without high bitterness. Highly carbonated like a Belgian Golden Strong ale, but even drier, and without Belgian spice and phenol character.", false, "IPA - Brut" },
                    { new Guid("bad58025-7855-482b-8f96-e74c4c122a9b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The English India Pale Ale (IPA) is a hoppy, moderately-strong, very well-attenuated pale golden to deep amber British ale with a dry finish and a hoppy aroma and flavor. Generally will have more finish hops and less fruitiness and/or caramel than British pale ales and bitter and has less hop intensity and a more pronounced malt flavor than typical American versions. The modern IPA style generally refers to American IPA and its derivatives but this does not imply that English IPA isn't a proper IPA. Originally, the attributes of IPA that were important to its arrival in good condition from England to India by ship were that it was very well-attenuated, and heavily hopped.", false, "IPA - English" },
                    { new Guid("ae339a73-e8cb-47f3-b250-a3d25c4cdedb"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Old Ale is an light amber to very dark reddish-brown colored English ale of moderate to fairly significant alcoholic strength, bigger than standard beers, though usually not as strong or rich as barleywine and often tilted towards a maltier balance. The predominant defining quality for this style is the impression of age, which can manifest itself in different ways (complexity, lactic, Brett, oxidation, leather, vinous qualities are some recurring examples). Roughly overlapping the British Strong Ale and the lower end of the English Barley Wine styles, but always having an aged quality. Barley Wines tend to develop more of an overall mature quality, while Old Ales can show more of the barrel qualities. Old Peculier are also considered as an Old Ale.", false, "Old Ale" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { new Guid("cf7b1d39-81a3-49ae-b5a5-40e2633c331b"), new Guid("43a08cc4-76ae-46d8-9e2c-cde7b0479146") },
                    { new Guid("8d421895-bdc9-46b0-8ced-f7d86bef8f24"), new Guid("cdde12b9-e61a-4748-a239-c7331b4fb6a8") }
                });

            migrationBuilder.InsertData(
                table: "Breweries",
                columns: new[] { "Id", "CountryId", "CreatedOn", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { new Guid("89e0215e-2726-489b-8d63-b851b997f622"), new Guid("eee1a9ab-c409-42c4-ae07-f622a959bb0b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "KamenitzaAD" },
                    { new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("eee1a9ab-c409-42c4-ae07-f622a959bb0b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "Beer Bastards - Papa Brewery - Burgas" },
                    { new Guid("70dc3f24-6681-4ca0-9a67-9e3e78fa8b93"), new Guid("eee1a9ab-c409-42c4-ae07-f622a959bb0b"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "Unknown" }
                });

            migrationBuilder.InsertData(
                table: "Beers",
                columns: new[] { "Id", "AlcoholPercent", "BreweryId", "CountryId", "CreatedOn", "Description", "ImageURL", "IsBeerOfTheWeek", "IsDeleted", "IsProduced", "IsPublished", "Mililiters", "Name", "Price", "StyleId" },
                values: new object[,]
                {
                    { new Guid("133e0d92-cedc-40a7-b8fd-e5669611b3dc"), 6.7000000000000002, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Clear medium yellow colour with a large, frothy, good lacing, mostly lasting, white head. Aroma is moderate malty, pale malt, moderate hoppy, flowers, citrus, light passion fruit. Flavor is moderate sweet and bitter with a long duration, passion fruit, fruity hops, citrus, fruity, pale malt. Body is medium, texture is oily to watery, carbonation is soft.", "https://birka.bg/wp-content/uploads/2018/10/baso.jpg", false, false, false, false, 0, "Basi Kefa", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("e13fe243-c8b3-494d-acca-39ea27cd8d41"), 6.0, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pours cloudy with a brief frothy head, plenty of lacing but no bead. Mid amber/ dirty orange in colour. Aromas of orange, banana and cereal. Very smooth in the mouth with flavours of banana, orange and pineapple over a base of honeyed cereal. Quite bitter in the finish, but scarcely any sweetness. Overall, another very enjoyable and well made Bulgarian IPA that is actually from Greece.", "https://cdn.dribbble.com/users/1621762/screenshots/4008898/dribble.png", false, false, false, false, 330, "Ailyak Cryo Mosaic", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("bacfedee-043c-4c26-b135-76cb2a092f1d"), 5.5, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Complex IPA, with rich aroma of citrus, pine, red fruit", "https://birka.bg/wp-content/uploads/2018/08/%D0%BE%D0%BF%D0%B0%D1%81%D0%B5%D0%BD.jpg", false, false, false, false, 0, "Opasen Char", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("631a4aff-5de7-4609-b7e1-6d8ba402adcc"), 5.2999999999999998, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Slightly cloudy, faint amber colour with average, frothy, slowoly osteoporosing, minimally lacing, white head. Caramel malty and citrusy, slightly leafy, hoppy aroma, hints of lemon tea, cautious doughy yeasty overtones. Taste is slightly dry, citrusy, minimally leafy, hoppy, minimally sweet caramel malty basis, hints of lemon tea, instant lemon tea, cautious smoky overtones; slightly overcarbonated.", "https://birka.bg/wp-content/uploads/2018/10/po.jpg", false, false, false, false, 0, "Po Poleka", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("b9c4fd4c-dd66-4418-add3-4e8199913413"), 5.5, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Unclear red, small foamy head. Rose hip, hibiscus lemon, tart notes, ice tea, grenadine, red currant Dry sourness. Medium bodied, oily watery texture, lively carbonated, tart dry sour finish. Lovely tasty beer.", "https://birka.bg/wp-content/uploads/2019/08/dirty.jpg", false, false, false, false, 0, "Freigeist Dirty Flamingo", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("4d65361e-c387-463c-b7ba-deff805032e5"), 4.5999999999999996, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), " Pours clear deep gold, almost pale amber with a thin fluffy head, some lacing but no visible rising carbonation. On the nose, brilliant ripe citrus - orange, grapefruit and tangerine. In the mouth, a touch of caramel and honey add to the citrus flavours, very slight astringency, minimal sweetness with a short - crisp - mid-bitter finish. An almost perfect IPL.", "https://birka.bg/wp-content/uploads/2019/04/bas.jpg", false, false, false, false, 355, "Bash Maistora", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("9af21ad7-d997-4fdb-b61e-8a32b780ef8e"), 5.7999999999999998, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Pours clear honey-gold with a fluffy, pure white head. Rather a nice pithy, citrusy pale ale, with some bread crust, semi-ripe citrus, nips of pine. Light bodied with fine to average carbonation. Well balanced finish. ", "https://birka.bg/wp-content/uploads/2019/11/%D0%B8%D0%BB%D0%BB.png", false, false, false, false, 0, "Faster Bastard IPA", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("d14758c8-3840-4b45-a861-bca2dca49de6"), 5.5, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), " Lightly hazy straw, heavy lacing but not much head or rising carbonation. On the nose, soft floral hops with bread and honey. In the mouth, soft rich flavours similar to the aromas, smooth texture, mild bitterness into a clean finish. A really good beer-flavoured beer.", "https://birka.bg/wp-content/uploads/2019/10/8.jpg", false, false, false, false, 0, "1 Vreme", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("781a1380-ad56-4717-94de-89fbe6997213"), 5.5, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Smoked chilli porter brewed with Rhombus Craft Brewery", "https://birka.bg/wp-content/uploads/2018/10/%D1%85%D0%BE.jpg", false, false, false, false, 0, "Hot Takova", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") },
                    { new Guid("e2e4bfd0-95e8-4b19-8666-731594961eb1"), 5.5, new Guid("3d046341-8215-453d-8647-cc5a63d039fb"), new Guid("7c1703b1-3f2a-4979-a9f0-7a176960c470"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "A stout brewed with bulgarian chestnut.", "https://birka.bg/wp-content/uploads/2018/11/da.png", false, false, false, false, 0, "Dami Kanyat", 0.0, new Guid("49657e0d-b39c-48ed-92ea-839e0f33afd7") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_BreweryId",
                table: "Beers",
                column: "BreweryId");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_CountryId",
                table: "Beers",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Beers_StyleId",
                table: "Beers",
                column: "StyleId");

            migrationBuilder.CreateIndex(
                name: "IX_Breweries_CountryId",
                table: "Breweries",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Reviews_BeerId",
                table: "Reviews",
                column: "BeerId");

            migrationBuilder.CreateIndex(
                name: "IX_Wishlist_BeerId",
                table: "Wishlist",
                column: "BeerId");

            migrationBuilder.CreateIndex(
                name: "IX_Wishlist_UserId",
                table: "Wishlist",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Reviews");

            migrationBuilder.DropTable(
                name: "Wishlist");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Beers");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Breweries");

            migrationBuilder.DropTable(
                name: "Styles");

            migrationBuilder.DropTable(
                name: "Countries");
        }
    }
}
