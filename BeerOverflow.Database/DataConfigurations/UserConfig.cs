﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Database.DataConfigurations
{
    public class UserConfig : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
            entity.Property(u => u.CreatedOn);

            entity.Property(u => u.IsBanned);

            //entity
            //    .HasMany(u => u.Reviews)
            //    .WithOne(r => r.User)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
