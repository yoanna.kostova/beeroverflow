﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Database.DataConfigurations
{
    public class CountryConfig : IEntityTypeConfiguration<Country>
    {
        public void Configure(EntityTypeBuilder<Country> entity)
        {
            entity.HasKey(c => c.Id);

            entity.Property(c => c.Name);

            entity.Property(r => r.CreatedOn);

            entity.Property(c => c.ISO);

            entity.Property(c => c.IsDeleted);

            //entity
            //    .HasMany(c => c.Breweries)
            //    .WithOne(br => br.Country)
            //    .OnDelete(DeleteBehavior.Cascade);

            //entity
            //    .HasMany(c => c.Beers)
            //    .WithOne(b => b.Country)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
