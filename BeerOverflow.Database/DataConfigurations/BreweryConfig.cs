﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Database.DataConfigurations
{
    public class BreweryConfig : IEntityTypeConfiguration<Brewery>
    {
        public void Configure(EntityTypeBuilder<Brewery> entity)
        {
            entity.HasKey(br => br.Id);

            entity.Property(br => br.Name);

            entity.Property(r => r.CreatedOn);

            entity.Property(br => br.IsDeleted);

            entity
                .HasOne(br => br.Country)
                .WithMany(c => c.Breweries)
                .HasForeignKey(b => b.CountryId)
               .OnDelete(DeleteBehavior.Restrict);

            //entity
            //    .HasMany(br => br.Beers)
            //    .WithOne(b => b.Brewery)
            //    .OnDelete(DeleteBehavior.Cascade);
            //    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
