﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Database.DataConfigurations
{
    public class ReviewConfig : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> entity)
        {
            entity.HasKey(r => r.Id);

            entity.Property(r => r.Content);

            entity.Property(r => r.CreatedOn);

            entity.Property(r => r.Flagged);

            entity.Property(r => r.IsBanned);

            entity.Property(r => r.Rating);

            entity.Property(br => br.IsDeleted);

            //entity
            //    .HasOne(r => r.User)
            //    .WithMany(u => u.Reviews)
            //    .HasForeignKey(x => x.UserId)
            //    .OnDelete(DeleteBehavior.Restrict);

            entity
                .HasOne(r => r.Beer)
                .WithMany(b => b.Reviews)
                .HasForeignKey(x => x.BeerId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
