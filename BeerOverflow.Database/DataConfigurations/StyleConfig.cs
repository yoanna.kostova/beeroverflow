﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Database.DataConfigurations
{
    public class StyleConfig : IEntityTypeConfiguration<Style>
    {
        public void Configure(EntityTypeBuilder<Style> entity)
        {
            entity.HasKey(s => s.Id);

            entity.Property(c => c.Name);

            entity.Property(r => r.CreatedOn);

            entity.Property(b => b.Description);

            entity.Property(r => r.IsDeleted);

            //entity
            //    .HasMany(s => s.Beers)
            //    .WithOne(b => b.Style)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
