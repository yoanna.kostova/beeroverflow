﻿using BeerOverflow.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Database.DataConfigurations
{
    public class BeerConfig : IEntityTypeConfiguration<Beer>
    {
        public void Configure(EntityTypeBuilder<Beer> entity)
        {
            entity.HasKey(b => b.Id);

            entity.Property(b => b.Name);

            entity.Property(b => b.Description);

            entity.Property(r => r.CreatedOn);

            entity.Property(b => b.Price);

            entity.Property(b => b.ImageURL);

            entity.Property(b => b.IsBeerOfTheWeek);

            entity.Property(b => b.IsProduced);

            entity.Property(b => b.AlcoholPercent);

            entity.Property(b => b.IsPublished);

            entity.Property(b => b.Mililiters);

            entity.Property(b => b.IsDeleted);

            entity
                .HasOne(b => b.Style)
                .WithMany(s => s.Beers)
                .HasForeignKey(b => b.StyleId)
                .OnDelete(DeleteBehavior.Restrict);


            entity
                .HasOne(b => b.Brewery)
                .WithMany(br => br.Beers)
                .HasForeignKey(b => b.BreweryId)
                //.OnDelete(DeleteBehavior.Cascade); 
                .OnDelete(DeleteBehavior.Restrict);

            entity
                .HasOne(b => b.Country)
                .WithMany(c => c.Beers)
                .HasForeignKey(b => b.CountryId)
                //.OnDelete(DeleteBehavior.Cascade);
                .OnDelete(DeleteBehavior.Restrict);

            //entity
            //    .HasMany(b => b.Reviews)
            //    .WithOne(r => r.Beer)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
