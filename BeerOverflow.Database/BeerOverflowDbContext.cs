﻿using BeerOverflow.Database.DataConfigurations;
using BeerOverflow.Database.Seeder;
using BeerOverflow.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace BeerOverflow.Database
{
    public class BeerOverflowDbContext : IdentityDbContext<User,Role,Guid>
    {
        public BeerOverflowDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Beer> Beers { get; set; }
        public DbSet<Brewery> Breweries { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Style> Styles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new BeerConfig());
            modelBuilder.ApplyConfiguration(new BreweryConfig());
            modelBuilder.ApplyConfiguration(new CountryConfig());
            modelBuilder.ApplyConfiguration(new ReviewConfig());
            modelBuilder.ApplyConfiguration(new StyleConfig());
            modelBuilder.ApplyConfiguration(new UserConfig());
            modelBuilder.ApplyConfiguration(new RoleConfig());

            modelBuilder.Seeder();

            base.OnModelCreating(modelBuilder);
        }
    }
}
