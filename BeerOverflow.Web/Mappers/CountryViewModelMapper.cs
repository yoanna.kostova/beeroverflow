﻿using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mappers
{
    public static class CountryViewModelMapper
    {
        public static CountryViewModel MapFrom(this CountryDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new CountryViewModel
            {
                Id = item.Id,
                ISO = item.ISO,
                 Name = item.Name
            };
        }

        public static ICollection<CountryViewModel> MapFrom(this ICollection<CountryDTO> items)
        {
            return items.Select(MapFrom).ToList();
        }

        public static CountryDTO MapFrom(this CountryViewModel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new CountryDTO
            {
                 Id = item.Id,
                 Name = item.Name,
                 ISO = item.ISO
            };
        }

        public static ICollection<CountryDTO> MapFrom(this ICollection<CountryViewModel> items)
        {
            return items.Select(MapFrom).ToList();
        }
    }
}
