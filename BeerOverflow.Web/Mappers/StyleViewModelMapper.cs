﻿
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mappers
{
    public static class StyleViewModelMapper 
    {
        public static StyleViewModel MapFrom(this StyleDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new StyleViewModel
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description
                
            };
        }

        public static ICollection<StyleViewModel> MapFrom(this ICollection<StyleDTO> items)
        {
            return items.Select(MapFrom).ToList();
        }

        public static StyleDTO MapFrom(this StyleViewModel item)
        {
            if (item==null)
            {
                throw new ArgumentNullException();
            }
            return new StyleDTO
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description

            };
        }

        public static ICollection<StyleDTO> MapFrom(this ICollection<StyleViewModel> items)
        {
            return items.Select(MapFrom).ToList();
        }

    }
}
