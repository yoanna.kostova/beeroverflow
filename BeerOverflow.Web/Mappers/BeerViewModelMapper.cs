﻿using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mappers
{
    public static class BeerViewModelMapper
    {
        public static BeerViewModel MapFrom(this BeerDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new BeerViewModel
            {
                Id = item.Id,
                Name = item.Name,
                 Description = item.Description,
                 Price = item.Price,
                 StyleName = item.StyleName,
                 BreweryName = item.BreweryName,
                 AlcoholPercent = item.AlcoholPercent,
                 CountryName = item.CountryName,
                 Mililiters = item.Mililiters,
                 ImageURL = item.ImageURL,
                 Reviews = item.Reviews?.MapFrom()
            };
        }
            public static ICollection<BeerViewModel> MapFrom(this ICollection<BeerDTO> items)
            {
                return items.Select(MapFrom).ToList();
            }
        public static BeerDTO MapFrom(this BeerViewModel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BeerDTO
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                Price = item.Price,
                StyleName =item.StyleName,
                BreweryName = item.BreweryName,
                AlcoholPercent = item.AlcoholPercent,
                CountryName=item.CountryName,
                ImageURL = item.ImageURL,
                Mililiters = item.Mililiters ,
            };
        }
        public static ICollection<BeerDTO> MapFrom(this ICollection<BeerViewModel> items)
        {
            return items.Select(MapFrom).ToList();
        }

    }
}
