﻿using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mappers
{
    public static class BreweryViewModelMapper
    {
        public static BreweryViewModel MapFrom(this BreweryDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new BreweryViewModel
            {
                Id = item.Id,
                Name = item.Name,
                CountryName = item.CountryName,
                Beers = item.Beers.MapFrom(),
                
            };
        }

        public static ICollection<BreweryViewModel> MapFrom(this ICollection<BreweryDTO> items)
        {
            return items.Select(MapFrom).ToList();
        }

        public static BreweryDTO MapFrom(this BreweryViewModel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new BreweryDTO
            {
                Id = item.Id,
                Name = item.Name,
                CountryName = item.CountryName,
                Beers = item.Beers?.MapFrom(),
                 
            };
        }

        public static ICollection<BreweryDTO> MapFrom(this ICollection<BreweryViewModel> items)
        {
            return items.Select(MapFrom).ToList();
        }
    }
}
