﻿using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mappers
{
    public static class UserViewModelMapper
    {
        public static UserViewModel MapFrom(this UserDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new UserViewModel
            {
                Id = item.Id,
                IsAdmin = item.IsAdmin,
                IsBanned = item.IsBanned,
                IsDeleted = item.IsDeleted,
                UserName = item.UserName,
                WishList = item.WishList
            };
        }

        public static ICollection<UserViewModel> MapFrom(this ICollection<UserDTO> items)
        {
            return items.Select(MapFrom).ToList();
        }

        public static UserDTO MapFrom(this UserViewModel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new UserDTO
            {
                Id = item.Id,
                UserName = item.UserName,
                IsDeleted = item.IsDeleted,
                IsBanned = item.IsBanned,
                IsAdmin = item.IsAdmin,


            };
        }

        public static ICollection<UserDTO> MapFrom(this ICollection<UserViewModel> items)
        {
            return items.Select(MapFrom).ToList();
        }
    }
}
