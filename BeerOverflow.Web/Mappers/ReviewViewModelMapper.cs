﻿using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Mappers
{
    public static class ReviewViewModelMapper
    {
        public static ReviewViewModel MapFrom(this ReviewDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new ReviewViewModel
            {
                Content = item.Content,
                BeerName = item.BeerName,
                Rating = item.Rating
            };
        }

        public static ICollection<ReviewViewModel> MapFrom(this ICollection<ReviewDTO> items)
        {
            return items.Select(MapFrom).ToList();
        }

        public static ReviewDTO MapFrom(this ReviewViewModel item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new ReviewDTO
            {
                Id = item.Id,
                BeerName = item.BeerName,
                Content = item.Content,
                Rating = item.Rating
            };
        }

        public static ICollection<ReviewDTO> MapFrom(this ICollection<ReviewViewModel> items)
        {
            return items.Select(MapFrom).ToList();
        }
    }
}
