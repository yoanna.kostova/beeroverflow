﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.Services;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Controllers
{
    public class WishlistController : Controller
    {
        private readonly IUserServices userServices;
        private readonly IWishlistServices wishlistServices;
        public WishlistController(IUserServices userServices, IWishlistServices wishlistServices)
        {
            this.userServices = userServices;
            this.wishlistServices = wishlistServices;
        }
        
        public IActionResult AddBeerToUserWishlist(string username, Guid beerId)
        {

            var user = userServices.GetUser(username);

            wishlistServices.AddBeertoWishlist(beerId, user.Id);


            return RedirectToAction("ListBeers","Beer");
        }
    }
}