﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Controllers
{
    public class StyleController : Controller
    {
        private readonly IStyleServices service;
        public StyleController(IStyleServices service)
        {
            this.service = service;
        }

        [HttpGet]
        public IActionResult ListStyles(StyleSearchModel model)
        {
            var styleList = service.GetAllStyles().ToList().MapFrom();

            if (!string.IsNullOrEmpty(model.Searcher))
            {
                styleList = styleList
                    .Where(s => s.Name.Contains(model.Searcher))
                    .ToList();
            }

            model.Styles = styleList;
            return View(model);
        }

        [HttpGet]
        public IActionResult CreateStyle()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateStyle(StyleViewModel styleViewModel)
        {
            var styleDTO = styleViewModel.MapFrom();

            service.CreateStyle(styleDTO);

            return RedirectToAction("ListStyles");
        }

        [HttpGet]
        public IActionResult DeleteStyle (Guid id)
        {
            service.DeleteStyle(id);


            return RedirectToAction("ListStyles");
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
            var style = this.service.GetStyle(id).MapFrom();
            return View(style);
        }


    }
}