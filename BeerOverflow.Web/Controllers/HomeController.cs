﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BeerOverflow.Web.Models;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;

namespace BeerOverflow.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IBeerServices beerService;

        public HomeController(ILogger<HomeController> logger, IBeerServices beerService)
        {
            _logger = logger;
            this.beerService = beerService;
        }

        public IActionResult Index()
        {
            var viewModel = new HomeViewModel();
            viewModel.BeersOfTheWeek = beerService.GetBeersOfTheWeek().MapFrom();

            return View(viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


    }
}
