﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using BeerOverflow.Services.Contracts;


namespace BeerOverflow.Web.Controllers
{
    public class BeerController : Controller
    {
        private readonly IBeerServices beerService;
        private readonly ICountryServices countryServices;
        private readonly IBreweryServices breweryServices;
        private readonly IStyleServices styleServices;
        public BeerController(IBeerServices beerService, ICountryServices countryServices, IBreweryServices breweryServices, IStyleServices styleServices)
        {
            this.beerService = beerService;
            this.countryServices = countryServices;
            this.breweryServices = breweryServices;
            this.styleServices = styleServices;
        }

        [HttpGet]
        public IActionResult CreateBeer()
        {
            ViewBag.Breweries = breweryServices.GetAllBreweries().MapFrom().ToList();
            ViewBag.Countries = countryServices.GetAllCountries().MapFrom().ToList();
            ViewBag.Styles = styleServices.GetAllStyles().MapFrom().ToList();

            return View();
        }

        [HttpPost]
        public IActionResult CreateBeer(BeerViewModel beerViewModel)
        {
            var beer = beerService.CreateBeer(beerViewModel.MapFrom());

            return RedirectToAction("ListBeers", "Beer");
        }

        [HttpGet]
        public IActionResult ListBeers(BeerSearchModel model,string sortOption)
        {
            var beerList = beerService.GetAllBeers().ToList().MapFrom();

            if (!string.IsNullOrEmpty(model.Searcher)&& !string.IsNullOrEmpty(model.SearchOption))
            {
                var searchOption = model.SearchOption;
                beerList = beerService.Search(beerList.MapFrom(), searchOption, model.Searcher).MapFrom();
               
            }
            if (sortOption != null)
            {
                beerList = beerService.Sort(beerList.MapFrom(),sortOption).MapFrom();
            }
            
            model.Beers = beerList;
            return View(model);
        }

        [HttpGet]
        public IActionResult DeleteBeer()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DeleteBeer(string name)
        {
            beerService.DeleteBeer(name);

            return RedirectToAction("ListBeers", "Beer");
        }

        [HttpGet]
        public IActionResult EditBeer(Guid id)
        {
            return View(beerService.GetBeerDTO(id).MapFrom());
        }

        [HttpPost]
        public IActionResult EditBeer(Guid id, string name)
        {

            this.beerService.EditBeer(id, name);
            return RedirectToAction("ListBeers");
        }
        [HttpGet]
        public IActionResult Details(Guid id)
        {
            var beer = this.beerService.GetBeerDTO(id).MapFrom();
            return View(beer);
        }
        [HttpGet]
        public IActionResult GetBeersOfTheWeek()
        {
            var list = beerService.GetBeersOfTheWeek().MapFrom();
            return View("_List", list);
        }
    }
}