﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace BeerOverflow.Web.Controllers
{
    public class BreweryController : Controller
    {
        private readonly IBreweryServices breweryServices;
        private readonly ICountryServices countryServices;
        public BreweryController(IBreweryServices breweryServices, ICountryServices countryServices)
        {
            this.breweryServices = breweryServices;
            this.countryServices = countryServices;
        }

        [HttpGet]
        public IActionResult CreateBrewery()
        {
            BreweryViewModel viewModel = new BreweryViewModel();

           // viewModel.Countries 
                ViewBag.Countries = countryServices.GetAllCountries().MapFrom().ToList();

            return View();
        }

        [HttpPost]
        public IActionResult CreateBrewery(BreweryViewModel breweryVM)
        {
            this.breweryServices.CreateBrewery(breweryVM.MapFrom());
            return RedirectToAction("ListBreweries");
        }

        [HttpGet]
        public IActionResult ListBreweries()
        {
            var breweries = this.breweryServices.GetAllBreweries().MapFrom().ToList();
            return View(breweries);
        }

        [HttpGet]
        public IActionResult DeleteBrewery()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DeleteBrewery(string name)
        {
            this.breweryServices.DeleteBrewery(name);

            return RedirectToAction("ListBreweries");
        }

        [HttpGet]
        public IActionResult EditBrewery(Guid id)
        {
            var brewery = this.breweryServices.GetBrewery(id).MapFrom();

            return View(brewery);
        }

        [HttpPost]
        public IActionResult EditBrewery(Guid id, string name)
        {
            breweryServices.EditBrewery(id, name);
            return RedirectToAction("ListBreweries");
        }

        [HttpGet]
        public IActionResult Details(Guid id)
        {
          var brewery = this.breweryServices.GetBrewery(id).MapFrom();
            return View(brewery);
        }
    }
}