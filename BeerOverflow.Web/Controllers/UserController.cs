﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Controllers
{
    public class UserController: Controller
    {
        private readonly IBeerServices beerService;
        private readonly ICountryServices countryServices;
        private readonly IBreweryServices breweryServices;
        private readonly IStyleServices styleServices;
        private readonly IUserServices userServices;
        public UserController(IBeerServices beerService, ICountryServices countryServices, IBreweryServices breweryServices, IStyleServices styleServices, IUserServices userServices)
        {
            this.beerService = beerService;
            this.countryServices = countryServices;
            this.breweryServices = breweryServices;
            this.styleServices = styleServices;
            this.userServices = userServices;
        }
        [HttpGet]
        public IActionResult Details(string username)
        {
            var user = this.userServices.GetUser(username);

            return View(user);
        }

        [HttpGet]
        public IActionResult ListUsers()
        {
            var users = this.userServices.GetAllUsers();

            var allUsers = new AllUsersViewModel();
            allUsers.Users = users.ToList();
            return View(allUsers);
        }

        [HttpGet]
        public IActionResult DeleteUser(Guid id)
        {
            this.userServices.DeleteUser(id);

            return RedirectToAction("ListUsers");
        }

        [HttpGet]
        public IActionResult BanUser(Guid id)
        {
            this.userServices.BanUser(id);

            return RedirectToAction("ListUsers");
        }
    }
}
