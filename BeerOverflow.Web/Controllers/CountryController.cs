﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Controllers
{
    public class CountryController : Controller
    {
        ICountryServices countryService;
        public CountryController(ICountryServices countryService)
        {
            this.countryService = countryService;
        }
        [HttpGet]
        public IActionResult CreateCountry()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateCountry(CountryViewModel countryViewModel)
        {
            var countryDTO = countryViewModel.MapFrom();

            countryService.CreateCountry(countryDTO);

            return RedirectToAction("ListCountry", "Country");
        }

        [HttpGet]
        public IActionResult ListCountry()
        {

            return View(countryService.GetAllCountries().MapFrom());
        }

        [HttpGet]
        public IActionResult DeleteCountry(Guid id)
        {
            countryService.DeleteCountry(id);

            return RedirectToAction("ListCountry");
        }

        [HttpGet]
        public IActionResult EditCountry(Guid id)
        {

            var country = countryService.GetCountry(id).MapFrom();

            return View(country);
        }

        [HttpPost]
        public IActionResult EditCountry(Guid id, string newName)
        {
            this.countryService.EditCountry(id, newName);

            return RedirectToAction("ListCountry");
        }
    }
}