﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Controllers
{
    public class ReviewController : Controller
    {
        private readonly IReviewServices reviewService;
        private readonly IBeerServices beerService;


        public ReviewController(IReviewServices reviewService, IBeerServices beerService)
        {
            this.reviewService = reviewService;
            this.beerService = beerService;
        }

        [HttpGet]
        public IActionResult CreateReview(Guid id)
        {
            var review = new ReviewViewModel();
            var beer = beerService.GetBeerDTO(id).MapFrom();
            review.BeerName = beer.Name;

            return View(review);
        }

        [HttpPost]
        public IActionResult CreateReview(ReviewViewModel review)
        {
            //var review = this.reviewService.GetReview(id).MapFrom();

            var reviewDTO = reviewService.CreateReview(review.MapFrom());

            Guid beerId = beerService.GetBeerDTO(reviewDTO.BeerName).Id;

            if (review == null || beerId == null)
            {
                throw new ArgumentNullException();
            }

            return RedirectToAction("Details","Beer",new { id = beerId});
        }

        [HttpGet]
        public IActionResult ListAllReviews()
        {
            var reviewList = reviewService.GetAllReviews()
                .ToList()
                .MapFrom();

            return View(reviewList);
        }

        [HttpGet]
        public IActionResult ListAllReviewsForBeer(Guid id)
        {
            var beer = beerService.GetBeerDTO(id);
            var reviewList = reviewService.GetAllReviewsForBeer(beer)
                .ToList()
                .MapFrom();

            return View(reviewList);
        }

        [HttpGet]
        public IActionResult DeleteReview()
        {
            return View();
        }

        [HttpPost]
        public IActionResult DeleteReview(Guid id)
        {
            reviewService.DeleteReview(id);

            return RedirectToAction("ListBeers", "Beer");
        }

        [HttpGet]
        public IActionResult EditReview(Guid id)
        {
            return View();
        }

        [HttpPost]
        public IActionResult UpdateReview(Guid id, string body)
        {
            reviewService.EditReview(id, body);

            return RedirectToAction("ListBeers", "Beer");
        }
    }
}