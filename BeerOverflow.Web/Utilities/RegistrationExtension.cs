﻿using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.Hashing;
using BeerOverflow.Services.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Utilities
{
    public static class RegistrationExtension
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IStyleServices, StyleServices>();
            services.AddScoped<ICountryServices, CountryServices>();
            services.AddScoped<IBeerServices, BeerServices>();
            services.AddScoped<IReviewServices, ReviewServices>();
            services.AddScoped<IBreweryServices, BreweryServices>();
            services.AddScoped<IUserServices, UserServices>();
            services.AddScoped<IWishlistServices, WishlistServices>();

            return services;
        }
    }
}
