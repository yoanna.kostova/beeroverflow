﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Database;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Areas.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewAPIController : ControllerBase
    {
        //TODO: CHECK REVIEW API - GET NOT WORKING

        private readonly IReviewServices reviewService;
        private readonly BeerOverflowDbContext context;
        public ReviewAPIController(IReviewServices reviewService, BeerOverflowDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));

            this.reviewService = reviewService ?? throw new ArgumentNullException(nameof(reviewService));
        }

        //GET: api/style
        [HttpGet]
        public IActionResult Get()
        {
            var reviewList = this.reviewService.GetAllReviews()
                .Select(x => new ReviewViewModel
                {
                    Id = x.Id,
                    Content = x.Content,
                    BeerName = x.BeerName,
                    Rating = x.Rating
                })
                .ToList();

            return Ok(reviewList);
        }

        //GET: api/style/{name}
        [HttpGet("{name}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var review = reviewService.GetReview(id);

                var reviewViewModel = new ReviewViewModel
                {
                    Id = review.Id,
                    Content = review.Content,
                    BeerName = review.BeerName,
                    Rating = review.Rating
                };

                return Ok(reviewViewModel);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        [HttpPost]
        public IActionResult Post(ReviewViewModel review)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }

                var reviewDTO = new ReviewDTO
                {
                    Id = review.Id,
                    Content = review.Content,
                    BeerName = review.BeerName,
                    Rating = review.Rating
                };

                this.reviewService.CreateReview(reviewDTO);

                //Get-because the new instance is already created and we are only getting it to return it, the second parameter is the input the get method requires
                return this.CreatedAtAction("Get", new { name = reviewDTO.Id }, reviewDTO.MapFrom());
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        [HttpPut]
        public IActionResult Delete(ReviewViewModel review)
        {
            var reviewDTO = reviewService.DeleteReview(review.Id);

            if (!ModelState.IsValid)
            {
                return this.NotFound();
            }

            return Ok(review);
        }
    }
}
