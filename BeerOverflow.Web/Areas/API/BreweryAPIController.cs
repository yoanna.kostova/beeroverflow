﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Areas.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class BreweryAPIController : ControllerBase
    {
        private readonly IBreweryServices breweryService;
        public BreweryAPIController(IBreweryServices breweryService)
        {
            this.breweryService = breweryService ?? throw new ArgumentNullException(nameof(breweryService));
        }

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            var brewList = breweryService.GetAllBreweries()
                .Select(x => new BreweryViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    CountryName = x.CountryName
                })
                .ToList();

            return Ok(brewList);
        }

        [HttpGet("{name}")]
        public ActionResult<BreweryDTO> Get(string name)
        {
            try
            {
                var brewery = breweryService.GetBrewery(name);
                var brewViewModel = new BreweryViewModel
                {
                    Id = brewery.Id,
                    Name = brewery.Name,
                    CountryName = brewery.CountryName
                };

                return Ok(brewViewModel);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post(BreweryViewModel brewery)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }
                var breweryDTO = new BreweryDTO
                {
                    Id = brewery.Id,
                    Name = brewery.Name,
                    CountryName = brewery.CountryName
                };

                this.breweryService.CreateBrewery(breweryDTO);

                return CreatedAtAction("Get", new { name = breweryDTO.Name }, breweryDTO.MapFrom());
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPut]
        public ActionResult<BreweryDTO> Delete(BreweryViewModel brewery)
        {
            var brew = breweryService.DeleteBrewery(brewery.Name);

            if (!ModelState.IsValid)
            {
                return NotFound();
            }

            return Ok(brewery);
        }
    }
}