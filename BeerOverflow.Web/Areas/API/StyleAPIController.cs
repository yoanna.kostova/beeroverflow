﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;


namespace BeerOverflow.Web.Areas.API
{
    //To be changed if we use these controllers for the views too
    //If wew will use thiss class for the views too, it should inherit from Controller!
    [Route("api/[controller]")]
    [ApiController]
    public class StyleAPIController : ControllerBase
    {
        private readonly IStyleServices styleService;
        private readonly BeerOverflowDbContext context;
        public StyleAPIController(IStyleServices styleService, BeerOverflowDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));

            this.styleService = styleService ?? throw new ArgumentNullException(nameof(styleService));
        }

        //GET: api/style
        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            var styleList = this.styleService.GetAllStyles()
                .Select(x => new StyleViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                })
                .ToList();

            return Ok(styleList);
        }

        //GET: api/style/{name}
        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            try
            {
                var style = styleService.GetStyle(name);

                var styleViewModel = new StyleViewModel
                {
                    Id = style.Id,
                    Name = style.Name,
                    Description = style.Description
                };

                return Ok(styleViewModel);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        // POST: api/style
        [HttpPost]
        [Route("")]
        public IActionResult Post(StyleViewModel style)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }

                var styleDTO = new StyleDTO
                {
                    Id = style.Id,
                    Name = style.Name,
                    Description = style.Description
                };

                styleService.CreateStyle(styleDTO);

                //Get-because the new instance is already created and we are only getting it to return it, the second parameter is the input the get method requires

                return this.CreatedAtAction("Get", new { name = styleDTO.Name }, styleDTO.MapFrom());
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        [HttpPut]
        public IActionResult Delete(StyleViewModel style)
        {
            var styleDTO = styleService.DeleteStyle(style.Id);
            if (!ModelState.IsValid)
            {
                return this.NotFound();
            }

            return Ok(style);
        }
    }
}