﻿using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountryAPIController : ControllerBase
    {
        private readonly ICountryServices countryService;
        public CountryAPIController(ICountryServices countryService)
        {
            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));
        }

        [HttpGet]
        [Route("")]

        public IActionResult Get()
        {
            var countryList = countryService.GetAllCountries()
                .Select(x => new CountryViewModel
                {
                    Id = x.Id,
                    ISO = x.ISO,
                    Name = x.Name
                })
                .ToList();

            return Ok(countryList);
        }

        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            try
            {
                var country = countryService.GetCountry(name);

                var countryViewModel = new CountryViewModel
                {
                    Id = country.Id,
                    ISO = country.ISO,
                    Name = country.Name
                };

                return Ok(countryViewModel);
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        [HttpPost]
        public IActionResult Post(CountryViewModel country)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }

                var countryDTO = new CountryDTO
                {
                    Id = country.Id,
                    ISO = country.ISO,
                    Name = country.Name
                };

                this.countryService.CreateCountry(countryDTO);

                return CreatedAtAction("Get", new { name = country.Name }, countryDTO.MapFrom());
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }
        [HttpPut]
        public IActionResult Delete(CountryViewModel country)
        {
            var countryDTO = countryService.DeleteCountry(country.Id);

            if (!ModelState.IsValid)
            {
                return this.NotFound();
            }
            return Ok(country);
        }

    }
}
