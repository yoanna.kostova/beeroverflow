﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.Services;
using BeerOverflow.Web.Mappers;
using BeerOverflow.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BeerOverflow.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class BeerAPIController : ControllerBase
    {
        private readonly IBeerServices beerService;
        public BeerAPIController(IBeerServices beerService)
        {
            this.beerService = beerService ?? throw new ArgumentNullException(nameof(beerService));
        }

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            var beerList = this.beerService.GetAllBeers()
                .Select(x => new BeerViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description,
                    Price = x.Price,
                    Reviews = x.Reviews.MapFrom(),
                    AlcoholPercent = x.AlcoholPercent,
                    Mililiters = x.Mililiters,
                    BreweryName = x.BreweryName,
                    CountryName = x.CountryName,
                    StyleName = x.StyleName
                })
                .ToList();

            return Ok(beerList);
        }

        [HttpGet("{name}")]
        public IActionResult Get(string name)
        {
            try
            {
                var beer = this.beerService.GetBeerDTO(name);

                var beerViewModel = new BeerViewModel
                {
                    Id = beer.Id,
                    Name = beer.Name,
                    Description = beer.Description,
                    Price = beer.Price,
                    Reviews = beer.Reviews.MapFrom(),
                    AlcoholPercent = beer.AlcoholPercent,
                    Mililiters = beer.Mililiters,
                    BreweryName = beer.BreweryName,
                    CountryName = beer.CountryName,
                    StyleName = beer.StyleName
                };

                return Ok(beerViewModel);
            }
            catch (Exception)
            {
                return this.NotFound();
            }    
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post(BeerViewModel beer)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return NotFound();
                }

                var beerDTO = new BeerDTO
                {
                    Id = beer.Id,
                    Name = beer.Name,
                    Description = beer.Description,
                    Price = beer.Price,
                    Reviews = beer.Reviews.MapFrom(),
                    AlcoholPercent = beer.AlcoholPercent,
                    Mililiters = beer.Mililiters,
                    BreweryName = beer.BreweryName,
                    CountryName = beer.CountryName,
                    StyleName = beer.StyleName
                };

                this.beerService.CreateBeer(beerDTO);

                return CreatedAtAction("Get", new { name = beerDTO.Name }, beerDTO.MapFrom());
            }
            catch (Exception)
            {
                return this.NotFound();
            }
        }

        [HttpPut]
        public IActionResult Delete(BeerViewModel beer)
        {
            var beerDTO = this.beerService.DeleteBeer(beer.Name);

            if (!ModelState.IsValid)
            {
                return this.NotFound();
            }

            return Ok(beer);
        }
    }
}