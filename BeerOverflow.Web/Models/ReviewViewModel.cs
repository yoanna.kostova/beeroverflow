﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class ReviewViewModel
    {
        public Guid Id { get; set; }
        public string BeerName { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
    }
}
