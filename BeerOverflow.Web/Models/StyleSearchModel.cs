﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class StyleSearchModel
    {
        public string Searcher { get; set; }
        public ICollection<StyleViewModel> Styles { get; set; }
    }
}
