﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BeerViewModel
    {
        public Guid Id { get; set; }
        [StringLength(40, MinimumLength = 2, ErrorMessage="Name length not valid")]
        public string Name { get; set; }
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Description length not valid")]
        public string Description { get; set; }
        public string ImageURL { get; set; }
        public double Price { get; set; }
        public string StyleName { get; set; }
        public string BreweryName { get; set; }

        [Range(1, 50, ErrorMessage = "Please enter value between {0} and {1}")]
        public double AlcoholPercent { get; set; }

        [StringLength(40, MinimumLength = 2, ErrorMessage = "Country is a mandatory field")]
        public string CountryName { get; set; }
        public int Mililiters { get; set; }
        public ICollection<ReviewViewModel> Reviews { get; set; }
    }
}
