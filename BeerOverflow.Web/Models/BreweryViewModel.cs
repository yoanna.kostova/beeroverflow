﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BreweryViewModel
    {
        public Guid Id { get; set; }
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Description length not valid")]
        public string Name { get; set; }
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Country is a mandatory field")]
        public string CountryName { get; set; }
        public ICollection<BeerViewModel> Beers { get; set; }

        [NotMapped]
        public List<CountryViewModel> Countries { get; set; }

    }
}
