﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class LoginViewModel
    {
        [MinLength(5)]
        [Display(Name = "Username")]
        public string LoginUsername { get; set; }

        [MinLength(5)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string LoginPassword { get; set; }

        [Display(Name = "Keep me logged in?")]
        public bool RememberLogin { get; set; }
    }
}
