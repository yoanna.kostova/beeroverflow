﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class WishlsitViewModel
    {

        public Guid Id { get; set; }
        public Guid BeerId { get; set; }
        public Guid UserId { get; set; }
        public string ImageURL { get; set; }
        public string BeerName { get; set; }
    }
}
