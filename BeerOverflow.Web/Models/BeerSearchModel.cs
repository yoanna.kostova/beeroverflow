﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class BeerSearchModel
    {
        public string Searcher { get; set; }
        public string SearchOption { get; set; }
        public ICollection<BeerViewModel> Beers { get; set; }
    }
}
