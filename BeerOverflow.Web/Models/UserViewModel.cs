﻿using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class UserViewModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public bool IsBanned { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<WishlistDTO> WishList { get; set; }
    }
}
