﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class HomeViewModel
    {
        public ICollection<BeerViewModel> BeersOfTheWeek { get; set; }
    }
}
