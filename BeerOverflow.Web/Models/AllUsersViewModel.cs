﻿using BeerOverflow.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class AllUsersViewModel
    {
        public ICollection<User> Users { get; set; }
    }
}
