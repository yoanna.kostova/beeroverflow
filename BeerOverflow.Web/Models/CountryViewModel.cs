﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BeerOverflow.Web.Models
{
    public class CountryViewModel
    {
        public Guid Id { get; set; }
        [StringLength(40, MinimumLength = 2, ErrorMessage = "Name length not valid")]
        public string Name { get; set; }
        public string ISO { get; set; }
    }
}
