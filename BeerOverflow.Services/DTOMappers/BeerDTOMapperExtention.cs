﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DTOMappers
{
    public static class BeerDTOMapperExtention
    {
        public static BeerDTO GetDTO(this Beer item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new BeerDTO
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                Price = item.Price,
                ImageURL = item.ImageURL,
                IsBeerOfTheWeek = item.IsBeerOfTheWeek,
                IsProduced = item.IsProduced,
                StyleId = item.StyleId,
                StyleName = item.Style?.Name,
                BreweryId = item.BreweryId,
                BreweryName = item.Brewery?.Name,
                AlcoholPercent = item.AlcoholPercent,
                IsPublished = item.IsPublished,
                Reviews = item.Reviews?.GetDTO(),
                CountryName = item.Country?.Name,
                Mililiters = item.Mililiters,
            };
        }

        public static ICollection<BeerDTO> GetDTO(this ICollection<Beer> items)
        {
            return items.Select(GetDTO).ToList();
        }

        public static Beer GetDTO(this BeerDTO item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new Beer
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                Price = item.Price,
                ImageURL = item.ImageURL,
                IsBeerOfTheWeek = item.IsBeerOfTheWeek,
                IsProduced = item.IsProduced,
                StyleId = item.StyleId,
                BreweryId = item.BreweryId,
                IsPublished = item.IsPublished,
                Mililiters = item.Mililiters,
            };
        }

        public static ICollection<Beer> GetDTO(this ICollection<BeerDTO> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
