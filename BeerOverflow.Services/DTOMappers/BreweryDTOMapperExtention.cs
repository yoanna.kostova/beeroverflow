﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DtoMappers
{
    public static class BreweryDTOMapperExtention
    {
        public static BreweryDTO GetDTO(this Brewery item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new BreweryDTO
            {
                Id = item.Id,
                Name = item.Name,
                CountryName = item.Country.Name,
                Beers = item.Beers?.GetDTO(),
                CountryId = item.CountryId,
            };
        }
        public static ICollection<BreweryDTO> GetDTO(this ICollection<Brewery> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
