﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DTOMappers
{
    public static class WishlistDTOMapperExtention
    {

        public static WishlistDTO GetDTO(this Wishlist item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new WishlistDTO
            {
                Id = item.Id,
                BeerId = item.BeerId,
                UserId = item.UserId,
                BeerName = item.Beer?.Name,
                ImageURL = item.Beer?.ImageURL
            };
        }
        public static ICollection<WishlistDTO> GetDTO(this ICollection<Wishlist> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
