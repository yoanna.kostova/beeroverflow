﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DTOMappers
{
    public static class CountryDTOMapperExtention
    {
        public static CountryDTO GetDTO(this Country item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            return new CountryDTO
            {
                Id = item.Id,
                Name = item.Name,
                ISO = item.ISO,
                Beers = item.Beers?.GetDTO(),
                Breweries = item.Breweries?.GetDTO(),
            };
        }
        public static ICollection<CountryDTO> GetDTO(this ICollection<Country> items)
        {
            return items.Select(GetDTO).ToList();
        }

    }

}
