﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DtoMappers
{
    public static class ReviewDTOMapperExtention
    {
        public static ReviewDTO GetDTO(this Review item)
        {
            var newReviewDTO = new ReviewDTO
            {
                Id = item.Id,
                BeerName = item.Beer?.Name,
                Content = item.Content,
                Flagged = item.Flagged,
                IsBanned = item.IsBanned,
                Rating = item.Rating
            };
            return newReviewDTO;
        }
        public static ICollection<ReviewDTO> GetDTO(this ICollection<Review> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
