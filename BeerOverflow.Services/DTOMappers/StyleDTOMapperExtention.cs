﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DtoMappers
{
    public static class StyleDTOMapperExtention
    {
        public static Style GetDTO(this StyleDTO item)
        {
            if (item == null)
            {
                throw new NotImplementedException();
            }
            return new Style
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description
            };
        }

        public static ICollection<StyleDTO> GetDTO(this ICollection<Style> items)
        {
            return items.Select(GetDTO).ToList();
        }

        public static StyleDTO GetDTO(this Style item)
        {
            if (item == null)
            {
                throw new NotImplementedException();
            }
            return new StyleDTO
            {
                Id = item.Id,
                Name = item.Name
            };
        }
    }
}
