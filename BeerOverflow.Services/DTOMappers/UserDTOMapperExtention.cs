﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.DtoMappers
{
    public static class UserDTOMapperExtention
    {
        public static UserDTO GetDTO(this User item)
        {
            if (item == null)
            {
                throw new ArgumentNullException();
            }

            return new UserDTO
            {
                Id = item.Id,
                IsDeleted = item.IsDeleted,
                IsBanned = item.IsBanned,
                UserName = item.UserName,
                //Reviews = item.Reviews?.GetDTO(),
                WishList = item.Wishlist?.GetDTO()
            };
        }
        public static ICollection<UserDTO> GetDTO(this ICollection<User> items)
        {
            return items.Select(GetDTO).ToList();
        }
    }
}
