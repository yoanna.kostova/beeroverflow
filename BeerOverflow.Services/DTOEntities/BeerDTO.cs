﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BeerOverflow.Services.DTOEntities
{
    public class BeerDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public string ImageURL { get; set; }
        public bool IsBeerOfTheWeek { get; set; }
        public bool IsProduced { get; set; }
        public double AlcoholPercent { get; set; }
        public bool IsPublished { get; set; }
        public int Mililiters { get; set; }
        public bool IsDeleted { get; set; }
        public Guid StyleId { get; set; }
        public string StyleName { get; set; }
        public Guid BreweryId { get; set; }
        public string BreweryName { get; set; }
        public Guid CountryId { get; set; }
        public string CountryName { get; set; }
        public ICollection<ReviewDTO> Reviews { get; set; }
    }
}
