﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTOEntities
{
    public class CountryDTO
    {
        //TODO: Do we need the guid id
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ISO { get; set; }
        public bool IsDeleted { get; set; }
        public ICollection<BreweryDTO> Breweries { get; set; }
        public ICollection<BeerDTO> Beers { get; set; }
        public ICollection<UserDTO> Users { get; set; }
    }
}
