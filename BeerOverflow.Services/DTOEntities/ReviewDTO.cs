﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTOEntities
{
    public class ReviewDTO 
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
        public int Flagged { get; set; }
        public bool IsBanned { get; set; }
        public int Rating { get; set; }
        //public Guid UserId { get; set; }

        //public Guid BeerId { get; set; }
        public string BeerName { get; set; }
    }
}
