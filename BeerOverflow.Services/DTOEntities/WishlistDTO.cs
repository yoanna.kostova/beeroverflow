﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTOEntities
{
    public class WishlistDTO
    {
        public Guid Id { get; set; }
        public Guid BeerId { get; set; }
        public Guid UserId { get; set; }
        public string BeerName { get; set; }
        public string ImageURL { get; set; }

    }
}
