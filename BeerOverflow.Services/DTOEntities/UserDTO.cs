﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.DTOEntities
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public bool IsBanned { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsDeleted { get; set; }

        //public ICollection<ReviewDTO> Reviews { get; set; }
        public ICollection<WishlistDTO> WishList { get; set; }

    }
}
