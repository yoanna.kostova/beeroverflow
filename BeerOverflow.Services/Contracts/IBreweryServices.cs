﻿using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.Services
{
    public interface IBreweryServices
    {
        bool AddBeerToBrewery(BeerDTO beerDTO, BreweryDTO breweryDTO);
        BreweryDTO CreateBrewery(BreweryDTO tempBrewery);
        BreweryDTO DeleteBrewery(string name);
        BreweryDTO EditBrewery(Guid Id, string name);
        ICollection<BreweryDTO> GetAllBreweries();
        BreweryDTO GetBrewery(string name);
        BreweryDTO GetBrewery(Guid Id);
        Guid GetId(string name);
       
    }
}