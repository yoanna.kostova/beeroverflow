﻿using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.Contracts

{
    public interface IStyleServices
    {
        bool AddBeerToStyle(StyleDTO styleDto, BeerDTO beerDto);
        StyleDTO CreateStyle(StyleDTO styleDto);
        StyleDTO DeleteStyle(Guid id);
        ICollection<StyleDTO> GetAllStyles();
        StyleDTO GetStyle(string name);
        Guid GetId(string name);
        StyleDTO GetStyle(Guid id);
    }
}