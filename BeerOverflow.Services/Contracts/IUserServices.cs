﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.Contracts
{
    public interface IUserServices
    {
        User DeleteUser(Guid id);
        string GetName(Guid id);
        User GetUser(Guid id);
        UserDTO GetUser(string name);
        ICollection<User> GetAllUsers();
        User BanUser(Guid id);
    }
}