﻿using BeerOverflow.Models;
using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.Services
{
    public interface IBeerServices
    {
        bool AddReviewToBeer(ReviewDTO reviewDTO, UserDTO beerDTO);
        BeerDTO CreateBeer(BeerDTO tempBeer);
        BeerDTO DeleteBeer(string name);
        BeerDTO EditBeer(Guid Id, string name);
        ICollection<BeerDTO> GetAllBeers();
        BeerDTO GetBeerDTO(string name);
        Beer GetBeer(Guid id);
        BeerDTO GetBeerDTO(Guid Id);
        Guid GetId(string name);
        ICollection<BeerDTO> GetBeersOfTheWeek();
        ICollection<BeerDTO> Sort(ICollection<BeerDTO> list, string sortOption);
        ICollection<BeerDTO> Search(ICollection<BeerDTO> list, string searchOption, string searcher);
    }
}