﻿using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.Contracts
{
    public interface IReviewServices
    {
        ReviewDTO CreateReview(ReviewDTO review);
        ICollection<ReviewDTO> GetAllReviews();
        ICollection<ReviewDTO> GetAllReviewsForBeer(BeerDTO beer);
        ReviewDTO GetReview(Guid id);
        ReviewDTO DeleteReview(Guid id);
        ReviewDTO EditReview(Guid id, string body);
    }
}