﻿using BeerOverflow.Services.DTOEntities;
using System;
using System.Collections.Generic;

namespace BeerOverflow.Services.Services
{
    public interface ICountryServices
    {
        bool AddBeerToCountry(BeerDTO beerDTO, CountryDTO countryDTO);
        bool AddBreweryToCountry(BreweryDTO breweryDTO, CountryDTO countryDTO);
        CountryDTO CreateCountry(CountryDTO tempCountry);
        CountryDTO DeleteCountry(Guid id);
        CountryDTO EditCountry(Guid Id, string name);
        ICollection<CountryDTO> GetAllCountries();
        CountryDTO GetCountry(string name);
        CountryDTO GetCountry(Guid id);
        Guid GetId(string name);
    }
}