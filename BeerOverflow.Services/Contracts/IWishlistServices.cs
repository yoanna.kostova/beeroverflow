﻿using BeerOverflow.Services.DTOEntities;
using System;

namespace BeerOverflow.Services.Services
{
    public interface IWishlistServices
    {
        WishlistDTO AddBeertoWishlist(Guid beerId, Guid userId);
    }
}