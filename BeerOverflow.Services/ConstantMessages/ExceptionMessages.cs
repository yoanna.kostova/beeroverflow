﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.ConstantMessages
{
    public static class ExceptionMessages
    {
        public const string GeneralOopsMessage = "Oops, something went wrong!";

        public const string EntityNull = "No entity found.";
        public const string DtoEntityNull = "No DTO found.";
        public const string EntityVmNull = "No View Model found.";

        public const string BeerNull = "No beer found.";
        public const string CountryNull = "No country found.";
        public const string BreweryNull = "No brewery found.";
        public const string StyleNull = "No style found.";
        public const string ReviewNull = "No review found.";
        public const string ListNull = "No items in our database found.";
        public const string UserNull = "No user found.";



        public const string BeerExists = "Beer already exists";
        public const string CountryExists = "Country already exists";
        public const string BreweryExists = "Brewery already exists";
        public const string StyleExists = "Style already exists";


        public const string ModelError = "Model state not valid.";




    }
}
