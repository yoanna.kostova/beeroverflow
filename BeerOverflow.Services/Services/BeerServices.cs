﻿using BeerOverflow.Database;

using BeerOverflow.Models;
using BeerOverflow.Services.ConstantMessages;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DTOMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Services
{
    //TODO: When a beer is deleted, it should be removed from all wish and drank lists from users, as well as from breweries and styles.
    public class BeerServices : IBeerServices
    {
        private readonly BeerOverflowDbContext context;
        private readonly IStyleServices styleService;
        private readonly ICountryServices countryService;
        private readonly IBreweryServices breweryService;

        public BeerServices(BeerOverflowDbContext context, IStyleServices styleService, ICountryServices countryService, IBreweryServices breweryService)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));

            this.styleService = styleService ?? throw new ArgumentNullException(nameof(styleService));

            this.countryService = countryService ?? throw new ArgumentNullException(nameof(countryService));

            this.breweryService = breweryService ?? throw new ArgumentNullException(nameof(breweryService));
        }

        public BeerDTO CreateBeer(BeerDTO tempBeer)
        {
            if (tempBeer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            else if (this.context.Beers.Any(c => c.Name == tempBeer.Name && c.IsDeleted == true))
            {
                throw new Exception(ExceptionMessages.BeerExists);
            }

            try
            {
                var beer = new Beer
                {
                    Id = tempBeer.Id,
                    Name = tempBeer.Name,
                    Description = tempBeer.Description,
                    Price = tempBeer.Price,
                    AlcoholPercent = tempBeer.AlcoholPercent,
                    Mililiters = tempBeer.Mililiters,
                    StyleId = this.styleService.GetId(tempBeer.StyleName),
                    BreweryId = this.breweryService.GetId(tempBeer.BreweryName),
                    CountryId = this.countryService.GetId(tempBeer.CountryName),
                    CreatedOn = DateTime.UtcNow,
                    IsBeerOfTheWeek = false
                };

                if (beer.ImageURL == null)
                {
                    beer.ImageURL = "https://res.cloudinary.com/teepublic/image/private/s--pw_sUnDh--/c_fit,g_north_west,h_840,w_728/co_191919,e_outline:40/co_191919,e_outline:inner_fill:1/co_ffffff,e_outline:40/co_ffffff,e_outline:inner_fill:1/co_bbbbbb,e_outline:3:1000/c_mpad,g_center,h_1260,w_1260/b_rgb:eeeeee/c_limit,f_jpg,h_630,q_90,w_630/v1511627003/production/designs/2100805_0.jpg";
                }

                this.context.Beers.Add(beer);
                this.context.SaveChanges();

                return tempBeer;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }

        }

        public BeerDTO EditBeer(Guid Id, string name)
        {
            var beer = this.context.Beers
                .FirstOrDefault(c => c.Id == Id && c.IsDeleted == false);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            try
            {
                beer.Name = name;

                this.context.Update(beer);
                this.context.SaveChanges();

                return beer.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public BeerDTO GetBeerDTO(string name)
        {
            var beer = this.context.Beers
                .Include(b => b.Country)
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .FirstOrDefault(b => b.Name == name && b.IsDeleted == false);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            return beer.GetDTO();
        }
        public Beer GetBeer(Guid id)
        {
            var beer = this.context.Beers
                .Include(b => b.Country)
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .Include(b => b.Wishlists)
                .FirstOrDefault(b => b.Id == id && b.IsDeleted == false);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            return beer;
        }

        public BeerDTO GetBeerDTO(Guid Id)
        {
            var beer = this.context.Beers
                .Include(b => b.Country)
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .Include(b => b.Reviews)
                .Include(b => b.Wishlists)
                .FirstOrDefault(b => b.Id == Id && b.IsDeleted == false);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            return beer.GetDTO();
        }

        //TODO: Maybe optimize it?
        public ICollection<BeerDTO> GetBeersOfTheWeek()
        {
            ICollection<BeerDTO> beers = this.context.Beers
                .Take(3).ToList().GetDTO();

            if (!beers.Any())
            {
                throw new Exception(ExceptionMessages.ListNull);
            }

            return beers;
        }

        public ICollection<BeerDTO> GetAllBeers()
        {
            ICollection<BeerDTO> beerList = this.context.Beers
                .Where(x => x.IsDeleted == false)
                .Include(b => b.Country)
                .Include(b => b.Style)
                .Include(b => b.Brewery)
                .ToList()
                .GetDTO();

            if (!beerList.Any())
            {
                throw new Exception(ExceptionMessages.ListNull);
            }

            return beerList;
        }

        public BeerDTO DeleteBeer(string name)
        {
            var beer = this.context.Beers
                .FirstOrDefault(c => c.Name == name && c.IsDeleted == false);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            beer.IsDeleted = true;

            this.context.Update(beer);
            this.context.SaveChanges();

            return beer.GetDTO();
        }

        public bool AddReviewToBeer(ReviewDTO reviewDTO, UserDTO beerDTO)
        {
            var beer = this.context.Beers
                .FirstOrDefault(c => c.Id == beerDTO.Id && c.IsDeleted == false);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            try
            {
                var review = context.Reviews
                .FirstOrDefault(b => b.Id == reviewDTO.Id);

                if (review == null)
                {
                    throw new Exception(ExceptionMessages.ReviewNull);
                }

                beer.Reviews.Add(review);

                this.context.Update(beer);
                this.context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public ICollection<BeerDTO> Sort(ICollection<BeerDTO> list, string sortOption)
        {
            switch (sortOption)
            {
                case "NameAsc":
                    list = list.OrderBy(b => b.Name).ToList();
                    break;

                case "NameDesc":
                    list = list.OrderByDescending(b => b.Name).ToList();
                    break;

                case "PriceAsc":
                    list = list.OrderBy(b => b.Price).ToList();
                    break;

                case "PriceDesc":
                    list = list.OrderByDescending(b => b.Price).ToList();
                    break;

                case "AlcoholPercentAsc":
                    list = list.OrderBy(b => b.AlcoholPercent).ToList();
                    break;

                case "AlcoholPercentDesc":
                    list = list.OrderByDescending(b => b.AlcoholPercent).ToList();
                    break;

                case "MililitersAsc":
                    list = list.OrderBy(b => b.Mililiters).ToList();
                    break;

                case "MililitersDesc":
                    list = list.OrderByDescending(b => b.Mililiters).ToList();
                    break;

                default:
                    break;
            }

            return list;
        }

        public ICollection<BeerDTO> Search(ICollection<BeerDTO> list, string searchOption, string searcher)
        {
            {
                switch (searchOption)
                {
                    case "Name":
                        list = list.Where(b => b.Name.Contains(searcher)).ToList();
                        break;

                    case "CountryName":
                        list = list.Where(b => b.CountryName.Contains(searcher)).ToList();
                        break;

                    case "BreweryName":
                        list = list.Where(b => b.BreweryName.Contains(searcher)).ToList();
                        break;

                    case "StyleName":
                        list = list.Where(b => b.StyleName.Contains(searcher)).ToList();
                        break;

                    default:
                        break;
                }
                return list;
            }
        }

        public Guid GetId(string name)
        {
            var beer = context.Beers.FirstOrDefault(b => b.Name == name);

            if (beer == null)
            {
                throw new Exception(ExceptionMessages.BeerNull);
            }

            return beer.Id;
        }

    }
}
