﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.ConstantMessages;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Services
{
    public class UserServices : IUserServices
    {
        private readonly BeerOverflowDbContext context;
        private readonly IBeerServices beerService;
        public UserServices(BeerOverflowDbContext context,IBeerServices beerService)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            
            this.beerService = beerService;
        }

        public User BanUser(Guid id)
        {
            var user = this.context.Users
                .FirstOrDefault(u => u.Id == id);

            if (user == null)
            {
                throw new Exception(ExceptionMessages.UserNull);
            }

            user.IsBanned = true;

            this.context.SaveChanges();

            return user;
        }

        public User DeleteUser(Guid id)
        {
            var user = this.context.Users
                .FirstOrDefault(u => u.Id == id);

            if (user == null)
            {
                throw new Exception(ExceptionMessages.UserNull);
            }

            user.IsDeleted = true;

            this.context.SaveChanges();

            return user;
        }

        public string GetName(Guid id)
        {
            return this.context.Users.FirstOrDefault(u => u.Id == id).UserName;
        }

        public User GetUser(Guid id)
        {
            if (id == null)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
            return this.context.Users.FirstOrDefault(u => u.Id == id);
        }

        public UserDTO GetUser(string username)
        {
            if (username == null)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
            var user = this.context.Users
                .Include(u => u.Wishlist)
                .FirstOrDefault(u => u.UserName == username).GetDTO();

            foreach (var item in user.WishList)
            {
                item.BeerName = beerService.GetBeer(item.BeerId).Name;
            }
            return user;
        }

        public ICollection<User> GetAllUsers()
        {
            ICollection<User> users = this.context.Users.ToList();

            return users;
        }
    }
}
