﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.ConstantMessages;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Services
{
    public class ReviewServices : IReviewServices
    {
        private readonly BeerOverflowDbContext context;
        private readonly IBeerServices beerService;


        public ReviewServices(BeerOverflowDbContext context, IBeerServices beerService)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(ReviewServices.context));
            this.beerService = beerService;
        }
        public ReviewDTO CreateReview(ReviewDTO review)
        {
            if (review == null)
            {
                throw new Exception(ExceptionMessages.ReviewNull);
            }
            //TODO: If a user already created a review, we should have a check here

            try
            {
                var newReview = new Review
                {
                    Id = Guid.NewGuid(),
                    BeerId = beerService.GetId(review.BeerName),
                    Rating = review.Rating,
                    Content = review.Content,
                    //UserId = null
                };

                this.context.Reviews.Add(newReview);
                //this.context.Update(newReview);
                this.context.SaveChanges();

                return review;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public ReviewDTO EditReview(Guid id, string body)
        {
            var review = this.context.Reviews
                .FirstOrDefault(r => r.Id == id && r.IsDeleted == false);

            if (review == null)
            {
                throw new Exception(ExceptionMessages.ReviewNull);
            }

            try
            {
                review.Content = body;

                this.context.Update(review);
                this.context.SaveChanges();

                return review.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public ReviewDTO GetReview(Guid id)
        {
            var review = this.context.Reviews
                //.Include(b => b.User)
                .FirstOrDefault(r => r.Id == id && r.IsDeleted == false);

            if (review == null)
            {
                throw new Exception(ExceptionMessages.ReviewNull);
            }

            return review.GetDTO();
        }

        public ICollection<ReviewDTO> GetAllReviews()
        {
            ICollection<Review> reviewsList = this.context.Reviews
                .Where(x => x.IsDeleted == false)
                .ToList();

            if (!reviewsList.Any())
            {
                throw new Exception(ExceptionMessages.ListNull);
            }

            return reviewsList.GetDTO();
        }

        public ReviewDTO DeleteReview(Guid id)
        {
            var review = this.context.Reviews
                .FirstOrDefault(r => r.Id == id);

            if (review == null)
            {
                throw new Exception(ExceptionMessages.ReviewNull);

            }

            review.IsDeleted = true;

            this.context.Update(review);
            this.context.SaveChanges();
            return review.GetDTO();
        }

        public ICollection<ReviewDTO> GetAllReviewsForBeer(BeerDTO beer)
        {
            ICollection<ReviewDTO> reviews = this.context.Reviews
                .Where(r => r.BeerId == beer.Id)
                .Include(r => r.Beer)
                .ToList()
                .GetDTO();

            return reviews;
        }
    }
}
