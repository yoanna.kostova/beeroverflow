﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.ConstantMessages;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Services
{
    public class BreweryServices : IBreweryServices
    {
        private readonly BeerOverflowDbContext context;

        public BreweryServices(BeerOverflowDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public BreweryDTO CreateBrewery(BreweryDTO tempBrewery)
        {
            if (tempBrewery == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }

            else if (this.context.Breweries.Any(c => c.Name == tempBrewery.Name && c.IsDeleted == true))
            {
                throw new Exception(ExceptionMessages.BreweryExists);
            }

            try
            {
                var brewery = new Brewery
                {
                    Id = Guid.NewGuid(),
                    Name = tempBrewery.Name,
                    CountryId = this.context.Countries
                                    .FirstOrDefault(c => c.Name == tempBrewery.CountryName).Id,
                };

                this.context.Breweries.Add(brewery);
                //this.context.Breweries.Update(brewery);
                this.context.SaveChanges();

                return tempBrewery;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public BreweryDTO EditBrewery(Guid Id, string name)
        {
            var brewery = this.context.Breweries
                .Include(c => c.Country)
                .FirstOrDefault(c => c.Id == Id && c.IsDeleted == false);

            if (brewery == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }

            try
            {
                brewery.Name = name;

                context.Update(brewery);
                context.SaveChanges();

                return brewery.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public BreweryDTO GetBrewery(string name)
        {
            var brewery = this.context.Breweries
                .Include(b => b.Country)
                .FirstOrDefault(b => b.Name == name && b.IsDeleted == false);

            if (brewery == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }

            return brewery.GetDTO();
        }

        public BreweryDTO GetBrewery(Guid id)
        {
            var brewery = this.context.Breweries
                .Include(b => b.Country)
                .Include(b => b.Beers)
                .FirstOrDefault(b => b.Id == id && b.IsDeleted == false);

            if (brewery == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }

            return brewery.GetDTO();
        }

        public ICollection<BreweryDTO> GetAllBreweries()
        {
            ICollection<Brewery> brewList = this.context.Breweries
                .Include(br => br.Country)
                .Where(x => x.IsDeleted == false)
                .ToList();

            if (!brewList.Any())
            {
                throw new Exception(ExceptionMessages.ListNull);
            }

            return brewList.GetDTO();
        }

        public BreweryDTO DeleteBrewery(string name)
        {
            var brewery = this.context.Breweries
                .FirstOrDefault(c => c.Name == name && c.IsDeleted == false);

            if (brewery == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }

            brewery.IsDeleted = true;

            this.context.Update(brewery);
            this.context.SaveChanges();
            return brewery.GetDTO();
        }

        public bool AddBeerToBrewery(BeerDTO beerDTO, BreweryDTO breweryDTO)
        {

            var brewery = this.context.Breweries
                .FirstOrDefault(c => c.Id == breweryDTO.Id && c.IsDeleted == false);

            if (brewery == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }

            try
            {
                var beer = this.context.Beers
                .FirstOrDefault(b => b.Id == beerDTO.Id && b.IsDeleted == false);

                if (beer == null)
                {
                    throw new Exception(ExceptionMessages.BeerNull);
                }

                brewery.Beers.Add(beer);
                this.context.Update(brewery);
                this.context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public ICollection<BreweryDTO> Sort(ICollection<BreweryDTO> list, string sortOption)
        {
            switch (sortOption)
            {

                case "NameAsc":
                    list = list.OrderBy(b => b.Name).ToList();
                    break;

                case "NameDesc":
                    list = list.OrderByDescending(b => b.Name).ToList();
                    break;

                default:
                    break;
            }

            return list;
        }

        public ICollection<BreweryDTO> Search(ICollection<BreweryDTO> list, string searchOption, string searcher)
        {
            {
                switch (searchOption)
                {
                    case "Name":
                        list = list.Where(b => b.Name.Contains(searcher)).ToList();
                        break;

                    case "CountryName":
                        list = list.Where(b => b.CountryName.Contains(searcher)).ToList();
                        break;

                    default:
                        break;
                }
                return list;
            }
        }

        public Guid GetId(string name)
        {
            var breweryId = this.context.Breweries.FirstOrDefault(b => b.Name == name);
            if (breweryId == null)
            {
                throw new Exception(ExceptionMessages.BreweryNull);
            }
            return breweryId.Id;
        }
    }
}
