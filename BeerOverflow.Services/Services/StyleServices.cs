﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.ConstantMessages;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using BeerOverflow.Services.DTOMappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BeerOverflow.Services.Services
{
    public class StyleServices : IStyleServices
    {
        private readonly BeerOverflowDbContext context;

        public StyleServices(BeerOverflowDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public StyleDTO CreateStyle(StyleDTO tempStyle)
        {
            if (tempStyle == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }
            else if (this.context.Styles.Any(c => c.Name == tempStyle.Name && c.IsDeleted == true))
            {
                throw new Exception(ExceptionMessages.StyleExists);
            }

            try
            {
                var style = new Style
                {
                    Id = tempStyle.Id,
                    Name = tempStyle.Name,
                    Description = tempStyle.Description
                };

                this.context.Styles.Add(style);
                this.context.SaveChanges();

                return tempStyle;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public StyleDTO EditStyle(Guid id, string name)
        {
            var style = this.context.Styles
                .FirstOrDefault(s => s.Id == id && s.IsDeleted == false);

            if (style == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }

            try
            {
                style.Name = name;

                this.context.Update(style);
                this.context.SaveChanges();

                return style.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public StyleDTO GetStyle(string name)
        {
            var style = this.context.Styles
                .Include(b => b.Beers)
                .FirstOrDefault(r => r.Name == name && r.IsDeleted == false);

            if (style == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }

            return style.GetDTO();
        }

        public StyleDTO GetStyle(Guid id)
        {
            var style = this.context.Styles
                .Include(b => b.Beers)
                .FirstOrDefault(r => r.Id == id && r.IsDeleted == false);

            if (style == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }

            return style.GetDTO();
        }

        public ICollection<StyleDTO> GetAllStyles()
        {
            ICollection<Style> styleList = this.context.Styles
                .Where(style => style.IsDeleted == false)
                .ToList();

            if (!styleList.Any())
            {
                throw new Exception(ExceptionMessages.ListNull);
            }

            return styleList.GetDTO();
        }

        public StyleDTO DeleteStyle(Guid id)
        {
            var style = this.context.Styles
                .FirstOrDefault(s => s.Id == id && s.IsDeleted == false);

            if (style == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }

            style.IsDeleted = true;

            this.context.Styles.Update(style);
            this.context.SaveChanges();

            return style.GetDTO();
        }

        public bool AddBeerToStyle(StyleDTO styleDto, BeerDTO beerDto)
        {
            var style = this.context.Styles
                .FirstOrDefault(s => s.Id == styleDto.Id && s.IsDeleted == false);

            if (style == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }

            try
            {
                var beer = this.context.Beers
                .FirstOrDefault(b => b.Id == beerDto.Id && b.IsDeleted == false);

                if (beer == null)
                {
                    throw new Exception(ExceptionMessages.BeerNull);
                }

                style.Beers.Add(beer);

                this.context.Update(style);
                this.context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public Guid GetId(string name)
        {
            var styleId = this.context.Styles
                .FirstOrDefault(s => s.Name == name);

            if (styleId == null)
            {
                throw new Exception(ExceptionMessages.StyleNull);
            }

            return styleId.Id;
        }
    }
}
