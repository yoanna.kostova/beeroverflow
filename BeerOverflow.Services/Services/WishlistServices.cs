﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.Contracts;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using BeerOverflow.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Text;

namespace BeerOverflow.Services.Services
{
    public class WishlistServices : IWishlistServices
    {
        private readonly BeerOverflowDbContext context;

        private readonly IUserServices userServices;
        private readonly IBeerServices beerServices;

        public WishlistServices(BeerOverflowDbContext context, IUserServices userServices, IBeerServices beerServices)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));

            this.userServices = userServices ?? throw new ArgumentNullException(nameof(userServices));
            this.beerServices = beerServices;

        }
       

        public WishlistDTO AddBeertoWishlist(Guid beerId, Guid userid)
        {
            var user = userServices.GetUser(userid);

            var wishlistItem = new Wishlist()
            {
                UserId = user.Id,
                BeerId = beerId,
            };

            

            user.Wishlist.Add(wishlistItem);

            context.Update(user);
            context.SaveChanges();

            return wishlistItem.GetDTO();

        }

    }
}
