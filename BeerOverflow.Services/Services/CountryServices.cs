﻿using BeerOverflow.Database;
using BeerOverflow.Models;
using BeerOverflow.Services.ConstantMessages;
using BeerOverflow.Services.DTOEntities;
using BeerOverflow.Services.DtoMappers;
using BeerOverflow.Services.DTOMappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeerOverflow.Services.Services
{
    public class CountryServices : ICountryServices
    {
        private readonly BeerOverflowDbContext context;
        public CountryServices(BeerOverflowDbContext context)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public CountryDTO CreateCountry(CountryDTO tempCountry)
        {
            if (tempCountry == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            else if (this.context.Countries.Any(c => c.Name == tempCountry.Name && c.IsDeleted == true))
            {
                throw new Exception(ExceptionMessages.CountryExists);
            }

            try
            {
                var country = new Country
                {
                    Id = tempCountry.Id,
                    Name = tempCountry.Name,
                    ISO = tempCountry.ISO
                };

                this.context.Countries.AddAsync(country);
                this.context.SaveChangesAsync();

                return tempCountry;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public CountryDTO EditCountry(Guid Id, string name)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.Id == Id && c.IsDeleted == false);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            try
            {
                country.Name = name;

                this.context.Update(country);
                this.context.SaveChangesAsync();

                return country.GetDTO();
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public CountryDTO GetCountry(string name)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.Name == name && c.IsDeleted == false);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            return country.GetDTO();
        }

        public CountryDTO GetCountry(Guid id)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.Id == id && c.IsDeleted == false);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            return country.GetDTO();
        }

        public ICollection<CountryDTO> GetAllCountries()
        {
            ICollection<Country> countryList = context.Countries
                .Where(x => x.IsDeleted == false)
                .ToList();

            if (!countryList.Any())
            {
                throw new Exception(ExceptionMessages.ListNull);
            }

            return countryList.GetDTO();
        }

        public CountryDTO DeleteCountry(Guid id)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.Id == id && c.IsDeleted == false);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            country.IsDeleted = true;

            this.context.Update(country);
            this.context.SaveChanges();

            return country.GetDTO();
        }

        public bool AddBreweryToCountry(BreweryDTO breweryDTO, CountryDTO countryDTO)
        {
            var country = context.Countries
                .FirstOrDefault(c => c.Id == countryDTO.Id && c.IsDeleted == false);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            try
            {
                var brewery = context.Breweries
               .FirstOrDefault(b => b.Id == breweryDTO.Id && b.IsDeleted == false);

                if (brewery == null)
                {
                    throw new Exception(ExceptionMessages.BreweryNull);
                }

                country.Breweries.Add(brewery);

                this.context.Update(country);
                this.context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public bool AddBeerToCountry(BeerDTO beerDTO, CountryDTO countryDTO)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.Id == countryDTO.Id && c.IsDeleted == false);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            try
            {
                var beer = this.context.Beers
                .Where(b => b.Id == beerDTO.Id && b.IsDeleted == false)
                .FirstOrDefault();

                if (beer == null)
                {
                    throw new Exception(ExceptionMessages.BeerNull);
                }
                country.Beers.Add(beer);

                this.context.Update(country);
                this.context.SaveChanges();

                return true;
            }
            catch (Exception)
            {
                throw new Exception(ExceptionMessages.GeneralOopsMessage);
            }
        }

        public ICollection<CountryDTO> Sort(ICollection<CountryDTO> list, string sortOption)
        {
            switch (sortOption)
            {

                case "NameAsc":
                    list = list.OrderBy(b => b.Name).ToList();
                    break;

                case "NameDesc":
                    list = list.OrderByDescending(b => b.Name).ToList();
                    break;

                default:
                    break;
            }
            return list;
        }

        public ICollection<CountryDTO> Search(ICollection<CountryDTO> list, string searchOption, string searcher)
        {
            {
                switch (searchOption)
                {
                    case "Name":
                        list = list.Where(b => b.Name.Contains(searcher)).ToList();
                        break;

                    default:
                        break;
                }

                return list;
            }
        }

        //TODO: Should we include IsDeleted property too?
        public Guid GetId(string name)
        {
            var country = this.context.Countries
                .FirstOrDefault(c => c.Name == name);

            if (country == null)
            {
                throw new Exception(ExceptionMessages.CountryNull);
            }

            return country.Id;
        }
    }
}
